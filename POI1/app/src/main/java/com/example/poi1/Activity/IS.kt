package com.example.poi1.Activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.poi1.CifradoTools
import com.example.poi1.Modelo.Registro
import com.example.poi1.R
import com.example.poi1.MisDatos
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener


class IS: AppCompatActivity() {

    /**Variables a usar para el FireBase**/
    private val database = FirebaseDatabase.getInstance()
    private val regRef = database.getReference("Usuarios")

    private fun enviarDatos(id: String){
        regRef.child(id).child("status").setValue("true")

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.iniciar_sesion)
        /**Obtener IDs**/
        val R_Correo = findViewById<EditText>(R.id.IS_Correo).text
        val R_Contraseña = findViewById<EditText>(R.id.IS_Contraseña).text
        val btnISC = findViewById<Button>(R.id.IS_Btn)
        btnISC.clearFocus()

        /**Evento boton**/
        btnISC.setOnClickListener {
            if(R_Contraseña.isNotEmpty() && R_Correo.isNotEmpty()){
                /**Conexion Firebase y checar si existe**/
                regRef.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(snapshot: DataSnapshot) {
                        var exist = false
                      for (snap in snapshot.children) {
                            val usuario: Registro = snap.getValue(
                                Registro::class.java
                            ) as Registro
                            if (usuario.Correo == R_Correo.toString() && usuario.Contraseña == R_Contraseña.toString()) {
                                exist = true
                                val intentPrincipalA = Intent(applicationContext, principal::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                enviarDatos(usuario.id)
                                intentPrincipalA.putExtra("nombre", usuario.Nombre)
                                intentPrincipalA.putExtra("id", usuario.id)
                                intentPrincipalA.putExtra("correo", usuario.Correo)
                                intentPrincipalA.putExtra("carrera", usuario.Carrera)
                                MisDatos.id=""
                                MisDatos.Nombre=""
                                MisDatos.Carrera=""
                                MisDatos.Correo=""
                                R_Correo.clear()
                                R_Contraseña.clear()
                                CifradoTools.cifradoYesOrNot = "true"
                                startActivity(intentPrincipalA)
                                finish()
                            }
                        }
                        if (exist == false){
                           // Toast.makeText(applicationContext, "Revise los campos", Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {

                    }
                })
            }
            else{
                Toast.makeText(this, "Introducir datos", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
package com.example.poi1.AcFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.Activity.CrearGrupo
import com.example.poi1.Activity.principal
import com.example.poi1.Adaptadores.ChatAdaptador
import com.example.poi1.Adaptadores.gruposAdaptador
import com.example.poi1.Adaptadores.usuarioAdaptador
import com.example.poi1.Modelo.Mensaje
import com.example.poi1.R
import com.example.poi1.MisDatos
import com.example.poi1.Modelo.Grupo
import com.example.poi1.Modelo.Registro
import com.google.firebase.database.*


class groups: Fragment(R.layout.fragment_6) {

    /**Variables para el uso de FireBase**/
    private val listaGrupos = mutableListOf<Grupo>()
    private val adapt =  gruposAdaptador(listaGrupos)
    private val database = FirebaseDatabase.getInstance()
    private val chatsRef = database.getReference("Grupo")

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        /**Vista es fragment6**/
        val view: View = inflater.inflate(R.layout.fragment_6_grupo, container, false)
        val rv : RecyclerView = view.findViewById(R.id.rv_mensajes)
        val creargrupo : Button = view.findViewById(R.id.AgregarGrupo)
        /**al rv se enlaza con el adaptador que es usuarioAdaptador, en el está la accion al momento de seleccionar algo del rv**/
        rv.adapter = adapt
        datos(view)
        creargrupo.setOnClickListener {
            val intentIS = Intent(activity, CrearGrupo::class.java)
            activity!!.startActivity(intentIS)
        }
        return view
    }

    private fun datos(view: View): View {
        val rv : RecyclerView = view.findViewById(R.id.rv_mensajes)
        /**Carga todos los usuarios que no sea uno mismo**/
        chatsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                listaGrupos.clear()
                for (snap in snapshot.children){
                    val grupo: Grupo = snap.getValue(
                            Grupo::class.java
                    ) as Grupo
                    if(grupo.carrera == MisDatos.Carrera){
                        listaGrupos.add(grupo)
                    }
                }
                if(listaGrupos.size > 0){
                    adapt.notifyDataSetChanged()
                    rv.smoothScrollToPosition(listaGrupos.size - 1)
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })
        return view
    }

    /**
     *  Fragment(R.layout.fragment_4)
    private val listMens = mutableListOf<Mensaje>()
    private  val adapt = ChatAdaptador(listMens)

    private val database = FirebaseDatabase.getInstance()
    private val chatsRef = database.getReference("Mensajes")



   //private  val rv = view.findViewById(R.id.rv_mensajes)


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_4, container, false)

        val rv : RecyclerView = view.findViewById(R.id.rv_mensajes)
        rv.adapter = adapt

        val txtmensaje : EditText = view.findViewById(R.id.edTxt_Mensaje)

        val btnAct: ImageButton = view.findViewById(R.id.btn_send_m)


        btnAct.setOnClickListener {

            val mensaje : String = txtmensaje.text.toString()
            if (mensaje.isNotEmpty()) {

                enviarMensaje(Mensaje("", mensaje,MisDatos.Nombre, "todos",ServerValue.TIMESTAMP))
                txtmensaje.text.clear()
            }
        }

        recibirMensajes(view)

        return view

    }

    private fun enviarMensaje(mensaje: Mensaje) {

        val mensajeFirebase = chatsRef.push()
        mensaje.id = mensajeFirebase.key ?: ""
        mensajeFirebase.setValue(mensaje)
    }

    private fun recibirMensajes(view: View): View {
        val rv : RecyclerView =  view.findViewById(R.id.rv_mensajes)

        chatsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                listMens.clear()
                for (snap in snapshot.children){
                    val mensaje: Mensaje = snap.getValue(
                            Mensaje::class.java
                    ) as Mensaje

                    if(mensaje.para=="todos"){
                        listMens.add(mensaje)
                    }

                }
                if(listMens.size > 0){
                    adapt.notifyDataSetChanged()
                    rv.smoothScrollToPosition(listMens.size - 1)
                }


            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
        return view
    }**/

}

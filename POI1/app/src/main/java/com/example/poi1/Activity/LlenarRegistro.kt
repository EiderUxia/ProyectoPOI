package com.example.poi1.Activity

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.poi1.Modelo.Registro
import com.example.poi1.R
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ServerValue


class LlenarRegistro : AppCompatActivity() {

    /**Variables a usar para el FireBase**/
    private val database = FirebaseDatabase.getInstance()
    private val regRef = database.getReference("Usuarios")

    /**Funcion para enviar datos a FireBase**/
    private fun enviarDatos(datos: Registro){
        val datosFirebase = regRef.push()
        datos.id = datosFirebase.key?:""
        datosFirebase.setValue(datos)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registrarse)

        /**Obtener la lista de las carreras**/
        val ListaCarreras = resources.getStringArray(R.array.opciones)
        /**Se colocan en un adaptador**/
        val adaptadorCarreras = ArrayAdapter(this, android.R.layout.simple_spinner_item, ListaCarreras)
        /**Se carga al spinner**/
        val spinner = findViewById<Spinner>(R.id.sp_Carreras)
        spinner.adapter = adaptadorCarreras

        /**Obtener los ids**/
        val R_Nombre = findViewById<EditText>(R.id.txtNombre).text
        val R_ApMaterno = findViewById<EditText>(R.id.txtApMaterno).text
        val R_ApPaterno = findViewById<EditText>(R.id.txtApPaterno).text
        val R_Correo = findViewById<EditText>(R.id.txtCorreo).text
        val R_Contraseña = findViewById<EditText>(R.id.txtContraseña).text
        val btnEnviar = findViewById<Button>(R.id.EnviarReg)
        var carrera = "ninguna"
        var aprovado = 0

        /**Obtener la carrera seleccionada**/
        spinner.onItemSelectedListener = object:
            AdapterView.OnItemSelectedListener{
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                carrera = ListaCarreras[position]
                aprovado = 1

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@LlenarRegistro, "Favor de seleccionar una carrera", Toast.LENGTH_SHORT).show()
                aprovado = 0
            }

        }

        /**Eventos al dar click en enviar**/
        btnEnviar.setOnClickListener {
            if(R_Nombre.isNotEmpty() && aprovado == 1){
               
               enviarDatos(Registro("", R_Nombre.toString(), R_ApPaterno.toString(),
                       R_ApMaterno.toString(), R_Correo.toString(), R_Contraseña.toString(),
                       carrera, ServerValue.TIMESTAMP))
                R_Nombre.clear()
                R_ApMaterno.clear()
                R_ApPaterno.clear()
                R_Correo.clear()
                R_Contraseña.clear()
                Toast.makeText(this, "Usuario guardado", Toast.LENGTH_SHORT).show()
            }
            else{
            Toast.makeText(this, "Debe llenar el campo", Toast.LENGTH_SHORT).show()
            }
        }

    }





}
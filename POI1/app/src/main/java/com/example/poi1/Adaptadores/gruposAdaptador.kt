package com.example.poi1.Adaptadores

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.Activity.mensaje_grupo
import com.example.poi1.Activity.mensaje_uno_a_uno
import com.example.poi1.Modelo.Grupo
import com.example.poi1.ObGrupo
import com.example.poi1.SusDatos
import com.example.poi1.R


class gruposAdaptador(private val listaGrupos: MutableList<Grupo>): RecyclerView.Adapter<gruposAdaptador.grupoViewHolder>(){
    class grupoViewHolder(itemViwe: View): RecyclerView.ViewHolder(itemViwe){
        fun asignarInformacion(grupo: Grupo) {
            /**Es la informacion que va a mostrar**/
            itemView.findViewById<TextView>(R.id.txt_u_nombre).text = grupo.nombre       }
        init {
            itemViwe.setOnClickListener {
                var position: Int = adapterPosition
                position += 1
                ObGrupo.posicion = position.toString()
                /**Al seleccionar un elemento abre el fragment7 que se encuentra en la carpeta de Activity**/
                val intent = Intent(itemViwe.context, mensaje_grupo::class.java)
                itemViwe.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): grupoViewHolder {
        return gruposAdaptador.grupoViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.usuarios, parent, false)
        )
    }

    override fun onBindViewHolder(holder: grupoViewHolder, position: Int) {
        holder.asignarInformacion(listaGrupos[position])
    }

    override fun getItemCount(): Int  = listaGrupos.size
}
package com.example.poi1.Adaptadores

import android.graphics.BitmapFactory
import android.text.Layout
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.MisDatos
import com.example.poi1.Modelo.Mensaje
import com.example.poi1.Modelo.MensajeGrupoVer
import com.example.poi1.R
import com.example.poi1.SusDatos
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*


class ChatGruposAdaptador(private val listaMensajesG: MutableList<MensajeGrupoVer>):
        RecyclerView.Adapter<ChatGruposAdaptador.ChatViewHolder>() {

    class ChatViewHolder(itemViwe:View): RecyclerView.ViewHolder(itemViwe){
        fun asignarInformacion(mensaje: MensajeGrupoVer) {
            itemView.findViewById<TextView>(R.id.txt_m_nombre).text = mensaje.nombre
            if (mensaje.contenido!="null"){
                itemView.findViewById<TextView>(R.id.txt_m_mensaje).text = mensaje.contenido
            }
            val dateFormater = SimpleDateFormat("dd/MM/yyyy - hh:mm:ss", Locale.getDefault())
            val fecha = dateFormater.format(Date(mensaje.timeStamp as Long))
            itemView.findViewById<TextView>(R.id.txt_m_fecha).text = fecha

            if(mensaje.imagen!="null"){
                val max = 5L *1024 * 1024
                val imRef =  FirebaseStorage.getInstance()
                var refe = imRef.reference.child("imagenes/${mensaje.imagen}").getBytes(max).toString()
                var bmp = BitmapFactory.decodeByteArray(refe.toByteArray(), 0, refe.toByteArray().size)
                itemView.findViewById<ImageView>(R.id.imageView).setImageBitmap(bmp)
            }


            val params = itemView.findViewById<LinearLayout>(R.id.contenedorMensaje).layoutParams
            if(mensaje.de == MisDatos.id){
                val newParams = FrameLayout.LayoutParams(
                    params.width,
                    params.height,
                    Gravity.END
                )
                itemView.findViewById<LinearLayout>(R.id.contenedorMensaje).layoutParams = newParams
            } else {
            val newParams = FrameLayout.LayoutParams(
                params.width,
                params.height,
                Gravity.START
            )
                itemView.findViewById<LinearLayout>(R.id.contenedorMensaje).layoutParams = newParams
            }

        }
    }
    //Cuantos elementos va a tener
    override fun getItemCount(): Int  = listaMensajesG.size

    //Como se va a ver
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return ChatViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.mensajes, parent, false)
        )
    }

    //Que va a tener
    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.asignarInformacion(listaMensajesG[position])
    }

}
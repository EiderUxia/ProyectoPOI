package com.example.poi1.Activity

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.*
import com.example.poi1.Adaptadores.ChatGruposAdaptador
import com.example.poi1.Modelo.Grupo
import com.example.poi1.Modelo.MensajeGrupoVer
import com.example.poi1.R
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.util.*


class mensaje_grupo : AppCompatActivity(){

    /**Variables para el FireBase en mensajes**/
    private val listaMensajes = mutableListOf<MensajeGrupoVer>()
    private val adaptador = ChatGruposAdaptador(listaMensajes)
    private val database = FirebaseDatabase.getInstance()
    private val mesRef = database.getReference("Mensajes")
    private val grRef = database.getReference("Grupo")
    private val listaGrupos = mutableListOf<Grupo>()
    private val coco = "Hey"
    private lateinit var filepath: Uri


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**Abre y selecciona el rv del fragment 7 que se encuentra en res -> layout**/
        setContentView(R.layout.fragment_7)
        /**Llama a la funcion de recibir mensajes**/
        recibirMensajes()
        /****/
        val rv = findViewById<RecyclerView>(R.id.rvh)
        rv.adapter = adaptador
        /** Obtiene los IDs y texto**/
        val txtmensaje = findViewById<EditText>(R.id.edTxt_Mensaje2)
        val btnAct = findViewById<ImageButton>(R.id.enviar)
        val btnImg = findViewById<ImageButton>(R.id.seleccImg)
        val mensaje = txtmensaje.text
        /**Evento al seleccionar enviar mensaje**/
        btnAct.setOnClickListener {
            if (mensaje.isNotEmpty()) {
                /**Llama a la funcion para enviar mensaje**/
                val cifrado: String
                if(CifradoTools.cifradoYesOrNot == "true"){
                    cifrado  = CifradoTools.cifrar(mensaje.toString(), coco)
                }
                else{
                    cifrado = mensaje.toString()
                }

                enviarMensaje(MensajeGrupoVer("", cifrado, MisDatos.id, ObGrupo.id, ServerValue.TIMESTAMP, MisDatos.Nombre, CifradoTools.cifradoYesOrNot))
               txtmensaje.text.clear()
            }
        }
        /**Evento al seleccionar enviar imagen**/
        btnImg.setOnClickListener {

                startFileChooser()
        }
    }

    private fun recibirMensajes() {
        val rv = findViewById<RecyclerView>(R.id.rvh)
        grRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                /**Busca al grupo que se selecciono en el rv**/
                var contador = 0
                listaGrupos.clear()
                for (snap in snapshot.children) {
                    val grupo: Grupo = snap.getValue(
                            Grupo::class.java
                    ) as Grupo
                    if (grupo.carrera == MisDatos.Carrera) {
                        contador += 1
                    }
                    if (contador == ObGrupo.posicion.toInt()) {
                        /**Se guarda en el objeto con el nombre Datos**/
                        ObGrupo.nombre = grupo.nombre
                        ObGrupo.id = grupo.id
                        listaGrupos.add(grupo)
                        break
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })
        /**Obtiene los mensajes del grupo y los propios**/
        mesRef.addValueEventListener(object : ValueEventListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onDataChange(snapshot: DataSnapshot) {
                listaMensajes.clear()
                for (snap in snapshot.children) {
                    val mensaje: MensajeGrupoVer = snap.getValue(
                            MensajeGrupoVer::class.java
                    ) as MensajeGrupoVer
                    if (mensaje.para == ObGrupo.id) {
                        val mensajeDes: String
                        if (mensaje.encriptado == "true")
                            mensajeDes = CifradoTools.descifrar(mensaje.contenido, coco)
                        else
                            mensajeDes = mensaje.contenido
                        mensaje.contenido = mensajeDes
                        listaMensajes.add(mensaje)
                    }
                }
                if (listaMensajes.size > 0) {
                    adaptador.notifyDataSetChanged()
                    rv.smoothScrollToPosition(listaMensajes.size - 1)
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }
        })


    }

    private fun enviarMensaje(mensaje: MensajeGrupoVer) {
        val mensajeFirebase = mesRef.push()
        mensaje.id = mensajeFirebase.key ?: ""
        mensajeFirebase.setValue(mensaje)
    }

    private fun startFileChooser(){
        var i = Intent()
        i.setType("image/*")
        i.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(i, "Escoge una imagen"), 111)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 111 && resultCode == Activity.RESULT_OK && data != null){
            filepath = data.data!!
            var pd = ProgressDialog(this)
            pd.setTitle("Uploading")
            pd.show()
            /**Enviarlo a irebase**/
            if (filepath != null){
                var aaaa : String
                val filname = UUID.randomUUID().toString()
                var ref = FirebaseStorage.getInstance().reference.child("imagenes/$filname")
                ref.putFile(filepath)
                        .addOnSuccessListener {p0 ->
                            Toast.makeText(applicationContext, "70 allá voy", Toast.LENGTH_SHORT).show()
                            aaaa = ref.path
                            enviarMensaje(MensajeGrupoVer("", "null", MisDatos.id, ObGrupo.id, ServerValue.TIMESTAMP, MisDatos.Nombre, "false", aaaa))
                        }
                        .addOnFailureListener {
                            p0 ->
                            Toast.makeText(applicationContext, "Segundas allá voy", Toast.LENGTH_SHORT).show()
                        }
                        .addOnProgressListener {
                            p0 ->
                            var progress: Double = (100.0 * p0.bytesTransferred) / p0.totalByteCount
                            pd.setMessage("Uploaded ${progress.toInt()}%")
                            if(progress.toInt() == 100)
                                pd.hide()
                        }

            }
        }
    }

    private fun uploadImageFB(){

    }

}

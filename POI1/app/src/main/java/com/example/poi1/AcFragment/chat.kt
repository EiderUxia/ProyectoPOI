
package com.example.poi1.AcFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.Adaptadores.usuarioAdaptador
import com.example.poi1.Modelo.Registro
import com.example.poi1.R
import com.example.poi1.MisDatos
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class chat: Fragment(R.layout.fragment_6) {
    /**Variables para el uso de FireBase**/
    private val listaUsuarios = mutableListOf<Registro>()
    private val adapt = usuarioAdaptador(listaUsuarios)
    private val database = FirebaseDatabase.getInstance()
    private val chatsRef = database.getReference("Usuarios")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        /**Vista es fragment6**/
        val view: View = inflater.inflate(R.layout.fragment_6, container, false)
        val rv : RecyclerView = view.findViewById(R.id.rv_mensajes)
        /**al rv se enlaza con el adaptador que es usuarioAdaptador, en el está la accion al momento de seleccionar algo del rv**/
        rv.adapter = adapt
        datos(view)
        return view
    }

    private fun datos(view: View): View {
        val rv : RecyclerView = view.findViewById(R.id.rv_mensajes)
        /**Carga todos los usuarios que no sea uno mismo**/
        chatsRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                listaUsuarios.clear()
                for (snap in snapshot.children){
                    val usuario: Registro = snap.getValue(
                        Registro::class.java
                    ) as Registro
                    if(usuario.id != MisDatos.id && usuario.Carrera == MisDatos.Carrera){
                        listaUsuarios.add(usuario)
                    }
                }
                if(listaUsuarios.size > 0){
                    adapt.notifyDataSetChanged()
                  rv.smoothScrollToPosition(listaUsuarios.size - 1)
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })
        return view
    }
}
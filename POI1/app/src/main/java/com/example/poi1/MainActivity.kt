package com.example.poi1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.poi1.Activity.IS
import com.example.poi1.Activity.LlenarRegistro
import com.example.poi1.Activity.principal

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(MisDatos.Nombre == ""){
            val btnReg = findViewById<Button>(R.id.btn_SignUp)
            val btnIS = findViewById<Button>(R.id.btn_LogIn)
            btnReg.setOnClickListener {
                val intentReg = Intent(this, LlenarRegistro::class.java)
                startActivity(intentReg)
            }
            btnIS.setOnClickListener {
                val intentIS = Intent(this, IS::class.java)
                startActivity(intentIS)
                finish()
            }
        }
        else{
            val intentIS = Intent(applicationContext, principal::class.java).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intentIS.putExtra("nombre", MisDatos.Nombre)
            intentIS.putExtra("id", MisDatos.id)
            intentIS.putExtra("correo", MisDatos.Correo)
            intentIS.putExtra("carrera", MisDatos.Carrera)
            startActivity(intentIS)
            finish()
        }
    }
}
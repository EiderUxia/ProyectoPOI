package com.example.poi1.Activity

import android.os.Build
import android.os.Bundle
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.SusDatos
import com.example.poi1.Modelo.Mensaje
import com.example.poi1.Modelo.Registro
import com.example.poi1.R
import com.example.poi1.MisDatos
import com.example.poi1.Adaptadores.ChatAdaptador
import com.example.poi1.CifradoTools
import com.google.firebase.database.*


class mensaje_uno_a_uno : AppCompatActivity(){

    /**Variables para el FireBase en mensajes**/
    private val listaMensajes = mutableListOf<Mensaje>()
    private val adaptador = ChatAdaptador(listaMensajes)
    private val database = FirebaseDatabase.getInstance()
    private val mesRef = database.getReference("Mensajes")
    private val usRef = database.getReference("Usuarios")
    private val listaUsuarios = mutableListOf<Registro>()
    private val coco = "Hey"

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /**Abre y selecciona el rv del fragment 7 que se encuentra en res -> layout**/
        setContentView(R.layout.fragment_7)
        val rv = findViewById<RecyclerView>(R.id.rvh)
        rv.adapter = adaptador
        /**Llama a la funcion de recibir mensajes**/
        recibirMensajes()
        /** Obtiene los IDs y texto**/
        val txtmensaje = findViewById<EditText>(R.id.edTxt_Mensaje2)
        val btnAct = findViewById<ImageButton>(R.id.enviar)
        val mensaje = txtmensaje.text
        /**Evento al seleccionar enviar mensaje**/
        btnAct.setOnClickListener {
            if (mensaje.isNotEmpty()) {
                /**Llama a la funcion para enviar mensaje**/
                val cifrado: String
                if(CifradoTools.cifradoYesOrNot == "true"){
                    cifrado  = CifradoTools.cifrar(mensaje.toString(), coco)
                }
                else{
                    cifrado = mensaje.toString()
                }
                enviarMensaje(Mensaje("",cifrado,MisDatos.id, SusDatos.ID, ServerValue.TIMESTAMP, CifradoTools.cifradoYesOrNot))
               txtmensaje.text.clear()
            }
        }
    }

    private fun recibirMensajes() {
        val rv = findViewById<RecyclerView>(R.id.rvh)
        usRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                /**Busca al usuario que se selecciono en el rv**/
               var contador = 0
                listaUsuarios.clear()
                for (snap in snapshot.children){
                    val usuario: Registro = snap.getValue(
                            Registro::class.java
                    ) as Registro
                    if(usuario.id != MisDatos.id && usuario.Carrera == MisDatos.Carrera){
                        contador+=1
                    }
                   if (contador == SusDatos.posicion.toInt()){
                       /**Se guarda en el objeto con el nombre Datos**/
                       SusDatos.Nombre = usuario.Nombre
                       SusDatos.ID = usuario.id
                       listaUsuarios.add(usuario)
                       break
                   }
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })
        /**Obtiene los mensajes de la persona y los propios**/
        mesRef.addValueEventListener(object : ValueEventListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onDataChange(snapshot: DataSnapshot) {
                listaMensajes.clear()
                for (snap in snapshot.children){
                    val mensaje: Mensaje = snap.getValue(
                        Mensaje::class.java
                    ) as Mensaje
                    if(mensaje.de == SusDatos.ID && mensaje.para == MisDatos.id || mensaje.de == MisDatos.id && mensaje.para == SusDatos.ID){
                        val desif: String
                        if (mensaje.encriptado == "true"){
                            desif = CifradoTools.descifrar(mensaje.contenido, coco)
                        }
                        else
                            desif = mensaje.contenido
                        mensaje.contenido = desif
                        listaMensajes.add(mensaje)
                    }

                }
                if(listaMensajes.size > 0){
                    adaptador.notifyDataSetChanged()
                    rv.smoothScrollToPosition(listaMensajes.size - 1)
                }
            }
            override fun onCancelled(error: DatabaseError) {
            }
        })
    }

    private fun enviarMensaje(mensaje: Mensaje) {
        val mensajeFirebase = mesRef.push()
        mensaje.id = mensajeFirebase.key ?: ""
        mensajeFirebase.setValue(mensaje)
    }



}

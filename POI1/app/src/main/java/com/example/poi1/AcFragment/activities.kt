package com.example.poi1.AcFragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.poi1.CifradoTools
import com.example.poi1.MainActivity
import com.example.poi1.MisDatos
import com.example.poi1.R
import com.google.firebase.database.FirebaseDatabase

class activities: Fragment(R.layout.fragment_1) {
    private val database = FirebaseDatabase.getInstance()
    private val csDB = database.getReference("Usuarios")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_1, container, false)
        val cerrarsesion : Button = view.findViewById(R.id.cerrar_sesion)
        val YesOrNot : Switch = view.findViewById(R.id.codif)
    YesOrNot.setOnClickListener{
        YesOrNot.isChecked()
    }
   YesOrNot.setOnCheckedChangeListener { buttonView, isChecked ->
       if (isChecked){
           CifradoTools.cifradoYesOrNot = "true"
       }
       else{
           CifradoTools.cifradoYesOrNot = "false"
       }
   }
        cerrarsesion.setOnClickListener {
           csDB.child(MisDatos.id).child("status").setValue("false")
            MisDatos.id=""
            MisDatos.Nombre=""
            MisDatos.Carrera=""
            MisDatos.Correo=""
            val intentMA = Intent(activity, MainActivity::class.java)
            activity!!.startActivity(intentMA)
            activity!!.finish()
        }

        return view
    }
}
package com.example.poi1.Activity

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.poi1.MisDatos
import com.example.poi1.Modelo.Grupo
import com.example.poi1.Modelo.Registro
import com.example.poi1.R
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ServerValue


class CrearGrupo : AppCompatActivity() {

    /**Variables a usar para el FireBase**/
    private val database = FirebaseDatabase.getInstance()
    private val regRef = database.getReference("Grupo")

    /**Funcion para enviar datos a FireBase**/
    private fun enviarDatos(datos: Grupo){
        val datosFirebase = regRef.push()
        datos.id = datosFirebase.key?:""
        datosFirebase.setValue(datos)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.crear_grupo)

        /**Obtener los ids**/
        val R_Nombre = findViewById<EditText>(R.id.txtNombre).text
        val btnEnviar = findViewById<Button>(R.id.EnviarReg)


        /**Eventos al dar click en enviar**/
        btnEnviar.setOnClickListener {
            if(R_Nombre.isNotEmpty()){
               
               enviarDatos(Grupo("", R_Nombre.toString(), MisDatos.Carrera, ServerValue.TIMESTAMP))
                R_Nombre.clear()
                Toast.makeText(this, "Grupo creado", Toast.LENGTH_SHORT).show()
            }
            else{
            Toast.makeText(this, "Debe llenar el campo", Toast.LENGTH_SHORT).show()
            }
        }

    }





}
package com.example.poi1.Modelo

import com.google.firebase.database.Exclude

class Mensaje(
        var id: String = "",
        var contenido: String = "",
        var de: String = "",
        var para: String = "",
        val timeStamp: Any? = null,
        val encriptado: String = ""
) {

}

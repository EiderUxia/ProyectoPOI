package com.example.poi1.Activity

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.poi1.AcFragment.*
import com.example.poi1.R
import com.example.poi1.MisDatos


class principal : AppCompatActivity(){

    private val bundle = Bundle()

    /**Funcion para abrir el Fragmento seleccionado**/
    fun cambiarFragmento(fragmentoNuevo: Fragment, tag: String, Nombre: String, ID: String,
                         Correo: String, Carrera: String){
        val fragmentoAnterior = supportFragmentManager.findFragmentByTag(tag)
        /**Envio nombre del usuario**/
        MisDatos.Nombre = Nombre
        MisDatos.id = ID
        MisDatos.Correo = Correo
        MisDatos.Carrera = Carrera
        if (fragmentoAnterior==null){
            val nuevo = fragmentoNuevo
            nuevo.arguments = bundle
            supportFragmentManager.beginTransaction().replace(R.id.contenedor, nuevo).commit()


        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.chat)
        /**Obtener el nombre del activity anterior**/
        val Nombre = intent.extras?.getString("nombre")
        val ID = intent.extras?.getString("id")
        val Correo = intent.extras?.getString("correo")
        val Carrera = intent.extras?.getString("carrera")
        /**Colocarlo en la barra superior**/
        supportActionBar?.title = Nombre
        /**Obtener ids**/
        val btnAct = findViewById<Button>(R.id.btn_activities)
        val btnArch = findViewById<Button>(R.id.btn_archive)
        val btnLlam = findViewById<Button>(R.id.btn_llamadas)
        val btnChat = findViewById<Button>(R.id.btn_chat)
        val btnGro = findViewById<Button>(R.id.btn_grupos)
        val btnWork = findViewById<Button>(R.id.btn_work)
        /**Dar click en algun boton**/
        btnAct.setOnClickListener {
            cambiarFragmento(activities(), "activities", Nombre.toString(), ID.toString(), Correo.toString(), Carrera.toString())
        }
        btnArch.setOnClickListener {
            cambiarFragmento(archive(), "archive", Nombre.toString(), ID.toString(), Correo.toString(), Carrera.toString())
        }
        btnLlam.setOnClickListener {
            cambiarFragmento(calls(), "calls", Nombre.toString(), ID.toString(), Correo.toString(), Carrera.toString())
        }
        btnChat.setOnClickListener {
            cambiarFragmento(chat(), "chat", Nombre.toString(), ID.toString(), Correo.toString(), Carrera.toString())
        }
        btnGro.setOnClickListener {
            cambiarFragmento(groups(), "groups", Nombre.toString(), ID.toString(), Correo.toString(), Carrera.toString())
        }
        btnWork.setOnClickListener {
            cambiarFragmento(work(), "work", Nombre.toString(), ID.toString(), Correo.toString(), Carrera.toString())
        }
    }



}
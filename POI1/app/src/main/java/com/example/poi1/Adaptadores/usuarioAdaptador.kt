package com.example.poi1.Adaptadores

import android.content.Intent
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.poi1.Activity.mensaje_uno_a_uno
import com.example.poi1.MisDatos
import com.example.poi1.SusDatos
import com.example.poi1.Modelo.Registro
import com.example.poi1.R


class usuarioAdaptador(private val listaUsuarios: MutableList<Registro>): RecyclerView.Adapter<usuarioAdaptador.usuarioViewHolder>(){
    class usuarioViewHolder(itemViwe: View): RecyclerView.ViewHolder(itemViwe){
        fun asignarInformacion(usuario: Registro) {
            /**Es la informacion que va a mostrar**/
            itemView.findViewById<TextView>(R.id.txt_u_nombre).text = usuario.Nombre
            val im = itemView.findViewById<ImageView>(R.id.status)
            if(usuario.status == "false"){
                im.setImageResource(R.drawable.ic_n)

            }
            else{
                im.setImageResource(R.drawable.ic_s)

            }

        }
        init {
            itemViwe.setOnClickListener {
                var position: Int = adapterPosition
                position += 1
                SusDatos.posicion = position.toString()
                /**Al seleccionar un elemento abre el Activity**/
                val intent = Intent(itemViwe.context, mensaje_uno_a_uno::class.java)
                itemViwe.context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): usuarioViewHolder {
        return usuarioAdaptador.usuarioViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.usuarios, parent, false)
        )
    }

    override fun onBindViewHolder(holder: usuarioViewHolder, position: Int) {
        holder.asignarInformacion(listaUsuarios[position])
    }

    override fun getItemCount(): Int  = listaUsuarios.size
}
package com.example.poi1.Modelo

import com.google.firebase.database.Exclude

class MensajeGrupoVer(
        var id: String = "",
        var contenido: String = "",
        var de: String = "",
        var para: String = "",
        val timeStamp: Any? = null,
        var nombre: String ="",
        val encriptado: String = "",
        val imagen: String ="null"
) {

}
